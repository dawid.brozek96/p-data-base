import os
import io
d = os.getcwd()
d1= os.path.join(d,'data')

for index in range(0, 100):
    name = "data{index}.txt".format(index=index)
    path = os.path.join(d1, name)
    with io.open(path, mode="r", encoding="utf-8") as fd:
        content = fd.read()
        w1 = os.path.join(d1, 'files')
        name2 = "file{index}.txt".format(index=index)
        w = os.path.join(w1, name2)
        fo = open(w, "w")
        fo.close()
        with open(path) as f:
            for line in f.read().splitlines():
                x, y = map(str, line.split(" ", 1))
                x = str(x).strip("file/.txt")
                a11, a12, a13, a21, a22, a23, a31, a32, a33, flag = map(float, y.split(" "))
                wyz = a11 * a22 * a33 + a12 * a23 * a31 + a21 * a32 * a13 - a31 * a22 * a13 - a21 * a12 * a33 - a11 * a32 * a23
                wyz = str(wyz)
                if flag == 0:
                    o = open(w, "a")
                    o.write("file" + x + " " + wyz + "\n")
                    o.close()
                else:
                    continue